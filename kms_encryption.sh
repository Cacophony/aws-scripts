#!/usr/bin/env bash

# Gives you two functions - One to encrypt, and one to decrypt with KMS.

# $1 - "A unique identifier for the customer master key":
#      - The key ID;
#      - The ARN of the key;
#      - The key alias (in the form alias/AliasName);
#      - The key alias ARN;
# Bear in mind that kms_encrypt_string has a limitation of 4096 characters.
function kms_encrypt_string() {
  if [ -t 0 ]; then
    >&2 echo "No data passed to kms_encrypt_string()."
    return 1
  fi
  aws kms encrypt \
    --key-id "${1}" \
    --plaintext "$(cat)" \
    --query "CiphertextBlob" \
    --output text \
    "${@:2}"
}
# e.g. kms_encrypt_string alias/my-key-alias <<< "lol"

# $1 - Key Identifier (similar to kms_encrypt_string above)
# $2 - Path to the target file to encrypt
function kms_encrypt_file() {
  if [ -z "${1// }" ]; then
    >&2 echo "Key identifier not passed to kms_encrypt_file()."
    return 1
  fi
  if [ -z "${2// }" ]; then
    >&2 echo "Path to file not passed to kms_encrypt_file()."
    return 1
  fi
  if [ ! -e "${2}" ]; then
    >&2 echo "File ${2} does not exist."
    return 2
  fi
  aws kms encrypt \
    --key-id "${1}" \
    --plaintext "fileb://${2}" \
    --query "CiphertextBlob" \
    --output text \
    "${@:3}"
}
# e.g. kms_encrypt_file alias/my-key-alias /tmp/my_config.yml > /tmp/my_config.yml.enc

function kms_decrypt() {
  if [ -t 0 ]; then
    >&2 echo "No data passed to kms_decrypt()."
    return 1
  else
    aws kms decrypt \
      --ciphertext-blob "fileb://"<(base64 --decode <<< "$(cat)") \
      --query "Plaintext" \
      --output text \
      "${@}" \
    | base64 --decode
  fi
}

# e.g. kms_decrypt < encrypted_config.yml > config.yml
